Ide dari website yang saya buat berasal dari Tugas MataKuliah PPW khususnya di story 4. Pada website tersebut terdapat 6 halaman, yaitu Homepage, About, Experience, Education, Skills, dan Contactt.
Untuk memudahkan berpindah halaman, terdapat hamburger menu di pojok kanan atas tiap halaman. Untuk backend saya menggunakan python--django dan uuntuk frontend saya menggunakan html,css, dan javascript.
 

Website saya bisa diakses online di link di bawah ini
## [my website](https://audreydatau.herokuapp.com)

Untuk menjalankan website secara lokal bisa melakukan langkah-langkah berikut:
1. Masuk ke folder HerokuStory4
2. buka terminal/cmd
3. jalankan "python manage.py runserver"
4. lalu akan diberikan url yang bisa langsung dibuka di browser masing- masing (http://127.0.0.1:8000/)