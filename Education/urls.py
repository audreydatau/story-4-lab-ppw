from django.urls import path
from . import views

app_name = 'Education'
urlpatterns = [
    path('', views.index, name="Education"),
]
