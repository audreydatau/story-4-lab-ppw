from django.shortcuts import render
from .models import create_comment
import datetime

def index(request):
    all_comment=create_comment.objects.all().order_by('time')
    context= {'all_comment': all_comment}
    if request.method == 'POST':
        comment=create_comment()
        comment.commentfield=request.POST.get('commentfield')
        comment.time=datetime.datetime.now()
        comment.save()
        return render(request, 'contact.html',context)
    else:
        return render(request, 'contact.html',context)
