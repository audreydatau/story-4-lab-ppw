from django.db import models

class create_comment(models.Model):
    commentfield = models.CharField(max_length=300)
    time = models.DateTimeField()

    def __str__(self):
        return self.commentfield