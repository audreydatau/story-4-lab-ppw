from django.urls import path
from . import views

app_name = 'Skills'
urlpatterns = [
    path('', views.index, name="Skills"),
]
