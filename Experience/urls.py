from django.urls import path
from . import views

app_name = 'Experience'
urlpatterns = [
    path('', views.index, name="Experience"),
]
